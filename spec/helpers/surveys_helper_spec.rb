require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the SurveysHelper. For example:
#
# describe SurveysHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe SurveysHelper, type: :helper do

  describe "verify rating option" do
    it "true if option param is there and in the list" do
      allow(self).to receive(:params).and_return({ survey_answer:
        { q1_answer: 3, q2_answer: 1, q3_answer: 2, q4_answer: "5", q5_answer: "4" }
      })

      expect(verify_rating_option :q1_answer).to eq(true)
      expect(verify_rating_option :q2_answer).to eq(true)
      expect(verify_rating_option :q3_answer).to eq(true)
      expect(verify_rating_option :q4_answer).to eq(true)
      expect(verify_rating_option :q5_answer).to eq(true)
    end

    it "false if option param is not there or its value in the list" do
      allow(self).to receive(:params).and_return({ survey_answer:
        { q1_answer: 0, q2_answer: 6, q3_answer: 10, q4_answer: -1, q5_answer: "swq" }
      })

      expect(verify_rating_option :q1_answer).to eq(false)
      expect(verify_rating_option :q2_answer).to eq(false)
      expect(verify_rating_option :q3_answer).to eq(false)
      expect(verify_rating_option :q4_answer).to eq(false)
      expect(verify_rating_option :q5_answer).to eq(false)
    end

    it "handles the absence of params for options" do
      allow(self).to receive(:params).and_return({ })

      expect(verify_rating_option :q1_answer).to eq(false)
    end
  end

  describe "check rating option" do
    it "In bool, indicates DOM radio elemet check when param att is equal it" do
      allow(self).to receive(:params).and_return({ survey_answer: { q2_answer: '2' } })

      expect(check_rating_option :q2_answer, 2).to eq(true)
    end

    it "In bool, indicates DOM radio elemet check when param att isn't equal it" do
      allow(self).to receive(:params).and_return({ survey_answer: { q2_answer: '2' } })

      expect(check_rating_option :q2_answer, 1).to eq(false)
    end
  end

  describe "check rating option for radio" do
    it "By html attribute svalue, indicates DOM radio elemet should be checked" do
      allow(self).to receive(:params).and_return({ survey_answer: { q2_answer: 2 } })

      expect(check_rating_option_for_radio :q2_answer, 2).to eq("checked")
    end

    it "By css class string, indicates DOM radio elemet should not be checked" do
      allow(self).to receive(:params).and_return({ survey_answer: { q2_answer: 2 } })

      expect(check_rating_option_for_radio :q2_answer, 1).to eq("")
    end
  end

  describe "check rating option in css" do
    it "By css class string, indicates DOM visual UI reflects checked element" do
      allow(self).to receive(:params).and_return({ survey_answer: { q2_answer: 2 } })

      expect(check_rating_option_in_css :q2_answer, 2).to eq("active")
    end

    it "By css class string, indicates DOM visual UI does not reflect checked element" do
      allow(self).to receive(:params).and_return({ survey_answer: { q2_answer: 2 } })

      expect(check_rating_option_in_css :q2_answer, 1).to eq("")
    end
  end
end
