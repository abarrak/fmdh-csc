# Preview all emails at http://localhost:3000/rails/mailers/survey_mailer
class SurveyMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/survey_mailer/invite_email
  def invite_email
    SurveyMailer.invite_email
  end

  # Preview this email at http://localhost:3000/rails/mailers/survey_mailer/thank_email
  def thank_email
    SurveyMailer.thank_email
  end

  # Preview this email at http://localhost:3000/rails/mailers/survey_mailer/fill_notification_email
  def fill_notification_email
    SurveyMailer.fill_notification_email
  end

end
