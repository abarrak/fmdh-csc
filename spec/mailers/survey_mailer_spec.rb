require "rails_helper"

RSpec.describe SurveyMailer, type: :mailer do
  describe "invite_email" do
    let(:mail) { SurveyMailer.invite_email }

    it "renders the headers" do
      expect(mail.subject).to eq("Invite email")
      expect(mail.to).to eq(["to@example.org"])
      expect(mail.from).to eq(["from@example.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

  describe "thank_email" do
    let(:mail) { SurveyMailer.thank_email }

    it "renders the headers" do
      expect(mail.subject).to eq("Thank email")
      expect(mail.to).to eq(["to@example.org"])
      expect(mail.from).to eq(["from@example.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

  describe "fill_notification_email" do
    let(:mail) { SurveyMailer.fill_notification_email }

    it "renders the headers" do
      expect(mail.subject).to eq("Fill notification email")
      expect(mail.to).to eq(["to@example.org"])
      expect(mail.from).to eq(["from@example.com"])
    end

    it "renders the body" do
      expect(mail.body.encoded).to match("Hi")
    end
  end

end
