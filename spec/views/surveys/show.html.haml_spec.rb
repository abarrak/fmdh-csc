require 'rails_helper'

RSpec.describe "surveys/show", type: :view do
  before(:each) do
    pending "Pending views tests #{__FILE__}"

    @survey = assign(:survey, Survey.create!(
      :fmdh_agent_name => "Fmdh Agent Name",
      :customer_name => "Customer Name",
      :customer_phone_number => "Customer Phone Number",
      :sr_number => 2,
      :customer_request_summary => "MyText",
      :filled => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Fmdh Agent Name/)
    expect(rendered).to match(/Customer Name/)
    expect(rendered).to match(/Customer Phone Number/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/false/)
  end
end
