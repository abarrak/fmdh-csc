require 'rails_helper'

RSpec.describe "surveys/index", type: :view do
  before(:each) do
    pending "Pending views tests #{__FILE__}"

    assign(:surveys, [
      Survey.create!(
        :fmdh_agent_name => "Fmdh Agent Name",
        :customer_name => "Customer Name",
        :customer_phone_number => "Customer Phone Number",
        :sr_number => 2,
        :customer_request_summary => "MyText",
        :filled => false
      ),
      Survey.create!(
        :fmdh_agent_name => "Fmdh Agent Name",
        :customer_name => "Customer Name",
        :customer_phone_number => "Customer Phone Number",
        :sr_number => 2,
        :customer_request_summary => "MyText",
        :filled => false
      )
    ])
  end

  it "renders a list of surveys" do
    render
    assert_select "tr>td", :text => "Fmdh Agent Name".to_s, :count => 2
    assert_select "tr>td", :text => "Customer Name".to_s, :count => 2
    assert_select "tr>td", :text => "Customer Phone Number".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
