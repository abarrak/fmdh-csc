require 'rails_helper'

RSpec.describe "surveys/new", type: :view do
  before(:each) do
    pending "Pending views tests #{__FILE__}"

    assign(:survey, Survey.new(
      :fmdh_agent_name => "MyString",
      :customer_name => "MyString",
      :customer_phone_number => "MyString",
      :sr_number => 1,
      :customer_request_summary => "MyText",
      :filled => false
    ))
  end

  it "renders new survey form" do
    render

    assert_select "form[action=?][method=?]", surveys_path, "post" do

      assert_select "input#survey_fmdh_agent_name[name=?]", "survey[fmdh_agent_name]"

      assert_select "input#survey_customer_name[name=?]", "survey[customer_name]"

      assert_select "input#survey_customer_phone_number[name=?]", "survey[customer_phone_number]"

      assert_select "input#survey_sr_number[name=?]", "survey[sr_number]"

      assert_select "textarea#survey_customer_request_summary[name=?]", "survey[customer_request_summary]"

      assert_select "input#survey_filled[name=?]", "survey[filled]"
    end
  end
end
