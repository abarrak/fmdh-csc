# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170105225049) do

  create_table "agents", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",        default: 0,     null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "username",                               null: false
    t.boolean  "admin",                  default: false, null: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.index ["admin"], name: "index_agents_on_admin"
    t.index ["email"], name: "index_agents_on_email", unique: true
    t.index ["reset_password_token"], name: "index_agents_on_reset_password_token", unique: true
    t.index ["username"], name: "index_agents_on_username"
  end

  create_table "surveys", force: :cascade do |t|
    t.string   "fmdh_agent_name",                          default: "",    null: false
    t.string   "customer_name",                            default: "",    null: false
    t.string   "customer_phone_number"
    t.integer  "sr_number",                                default: 0,     null: false
    t.datetime "request_first_notification_date_and_time"
    t.datetime "initial_reply_email_date_and_time"
    t.datetime "request_resolution_date"
    t.text     "customer_request_summary"
    t.boolean  "filled",                                   default: false, null: false
    t.datetime "filled_at"
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
    t.string   "unique_id",                                default: "",    null: false
    t.string   "customer_email",                           default: "",    null: false
    t.text     "answer"
    t.integer  "customer_job_title"
    t.integer  "hits_counter",                             default: 0,     null: false
    t.index ["customer_email"], name: "index_surveys_on_customer_email"
    t.index ["customer_name"], name: "index_surveys_on_customer_name"
    t.index ["filled"], name: "index_surveys_on_filled"
    t.index ["filled_at"], name: "index_surveys_on_filled_at"
    t.index ["fmdh_agent_name"], name: "index_surveys_on_fmdh_agent_name"
    t.index ["sr_number"], name: "index_surveys_on_sr_number"
    t.index ["unique_id"], name: "index_surveys_on_unique_id"
  end

end
