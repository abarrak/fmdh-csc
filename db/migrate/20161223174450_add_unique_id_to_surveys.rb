class AddUniqueIdToSurveys < ActiveRecord::Migration[5.0]
  def change
    add_column :surveys, :unique_id, :string, null: false, default: ""
    add_index :surveys, :unique_id
  end
end
