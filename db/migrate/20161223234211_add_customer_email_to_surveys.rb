class AddCustomerEmailToSurveys < ActiveRecord::Migration[5.0]
  def change
    add_column :surveys, :customer_email, :string, null: false, default: ""
    add_index :surveys, :customer_email
  end
end
