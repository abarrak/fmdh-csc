class AddCustomerJobTitleToSurveys < ActiveRecord::Migration[5.0]
  def change
    add_column :surveys, :customer_job_title, :integer
  end
end
