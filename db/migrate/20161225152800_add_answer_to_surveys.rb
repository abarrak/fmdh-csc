class AddAnswerToSurveys < ActiveRecord::Migration[5.0]
  def change
    add_column :surveys, :answer, :text
  end
end
