class AddHitsCounterToSurveys < ActiveRecord::Migration[5.0]
  def change
    add_column :surveys, :hits_counter, :integer, default: 0, null: false
  end
end
