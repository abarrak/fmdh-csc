class CreateSurveys < ActiveRecord::Migration[5.0]
  def change
    create_table :surveys do |t|
      t.string :fmdh_agent_name, null: false, default: ""
      t.string :customer_name, null: false, default: ""
      t.string :customer_phone_number
      t.integer :sr_number, null: false, default: 0
      t.datetime :request_first_notification_date_and_time
      t.datetime :initial_reply_email_date_and_time
      t.datetime :request_resolution_date
      t.text :customer_request_summary
      t.boolean :filled, null: false, default: false
      t.datetime :filled_at

      t.timestamps
    end

    add_index :surveys, :fmdh_agent_name
    add_index :surveys, :customer_name
    add_index :surveys, :sr_number
    add_index :surveys, :filled
    add_index :surveys, :filled_at
  end
end
