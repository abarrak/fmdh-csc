# Set up agents team & admin inital accounts.
Agent.first_or_create [
  { username: "fmhd", email: "fmhd@ex.com", password: "112233", password_confirmation: "112233" },
  { username: "fmhd-admin", email: "fmhd-admin@ex.com", password: "112233",
    password_confirmation: "112233", admin: true }
]

# Seeding some test surveys
Survey.first_or_create [
  {  fmdh_agent_name: "Yahya Samer", customer_name: "Ahmed Someone",
      customer_phone_number: "0555555555", sr_number: 3000, customer_job_title: :mr,
      request_first_notification_date_and_time: (DateTime.yesterday - 1),
      initial_reply_email_date_and_time: (DateTime.yesterday - 1),
      request_resolution_date: DateTime.now,
      customer_request_summary: "Water Leaking in QM-01 Building", filled: true,
      filled_at: DateTime.now, unique_id: "8kLuAEif", customer_email: "ahmed@company.com",
      answer: { "q1_answer" => "4", "q2_answer" => "5", "q3_answer" => "4", "q4_answer"=>"3",
                "q5_answer" => "2", "suggestions_and_notes" => "none." }
  },
  {  fmdh_agent_name: "Saeed", customer_name: "Khan Amir", customer_job_title: :mr,
      customer_phone_number: "0555000000", sr_number: 3990,
      request_first_notification_date_and_time: (DateTime.yesterday - 1),
      initial_reply_email_date_and_time: (DateTime.yesterday - 1),
      request_resolution_date: DateTime.now,
      customer_request_summary: "Water Leaking in QM-01 Building", unique_id: "YkLuXEif",
      customer_email: "khan@lool-company.com"
  },
  {  fmdh_agent_name: "Sara Salem", customer_name: "Khan Amir", sr_number: 4500,
      request_first_notification_date_and_time: (DateTime.yesterday - 1),
      initial_reply_email_date_and_time: (DateTime.yesterday - 1),
      request_resolution_date: DateTime.now, customer_job_title: :mr,
      customer_request_summary: "Water Leaking in QM-01 Building", unique_id: "5555XEiP",
      customer_email: "khan@lool-company.com"
  }
]
