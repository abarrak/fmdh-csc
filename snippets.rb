# No need for checking existance for unique_id ..
# Before_validation callback and the uniqueness validation ensures no collisions
# ..
# Besides SecureRandom highly unlikely to have that in the first place ..
def generate_unique_id
  if self[:unique_id].nil? || self[:unique_id].length < 6
    generated_id = ""
    loop do
      generated_id = random_token
      puts 'Runned Once -----------------'
      break if Survey.where(unique_id: generated_id).count == 0
    end
    self[:unique_id] = generated_id
  end
end


# Disbale Turbolinks for an a element
'data-turbolinks' => 'false'


# lock entire record
def readonly?
  filled? ? true : false
end


## Bredcrumb for customers .. not needed .. drop them.
# from fill page
%nav
  %ul.breadcrumb
    %li= link_to t('home'), root_path
    %li= link_to survey_breadcrumb_title(@survey), preview_path(@survey)
    %li.active= t 'surveys.preview_and_fill'

# from thank
%nav
  %ul.breadcrumb
    %li= link_to t('home'), root_path
    %li= link_to survey_breadcrumb_title(@survey), preview_path(@survey)
    %li.active= t 'surveys.thank_you'

# Heroku reset & migrate
heroku pg:reset DATABASE_URL
heroku rake db:migrate

# Heroku restart
heroku restart

# Heroku quota
heroku ps -a fmhd-customer-surveys-center

# Lots of beatiful things in rails .. bless it and these bloggers and commenters.
# rethrowexception.wordpress.com/2011/02/19/rails-link_to-current-page/
# stackoverflow.com/questions/2543576/rails-link-to-current-page-and-passing-parameters-to-it
