Rails.application.routes.draw do
  # root page routes.
  get "/:locale" => 'pages#home', as: "locale_root"
  root to: "pages#home"

  # RESTful authentication router.
  devise_for :agents, path: "accounts",
             path_names: { sign_in: "login", sign_out: "logout", sign_up: "register" }

  # I18n routing.
  scope "(:locale)", locale: /en|ar/ do
    # Named static pages routes.
    get "contact" => "pages#contact"

    # REST Resources routes.
    resources :surveys do
      get  "invitation" => "surveys#invitation", on: :member
      post "invitation" => "surveys#invite", on: :member

      collection do
        get   "preview/:unique_id/" => "surveys#preview", as: "preview"
        patch "preview/:unique_id/" => "surveys#fill", as: "fill"
        get   ":unique_id/thank"    => "surveys#thank", as: "thank"
      end
    end

    # Dashboard Routes.
    scope :dashboard do
      get "main"     => "dashboard#main", as: "dashboard_main"
      get "report"   => "dashboard#report", as: "dashboard_report"
      get "stats"    => "dashboard#stats", as: "dashboard_stats"
      get "settings" => "dashboard#settings", as: "dashboard_settings"
    end
  end
end
