Rails.application.config.action_mailer.delivery_method = :smtp
Rails.application.config.action_mailer.smtp_settings = {
  :address              => 'smtp.office365.com',
  :port                 => '587',
  :authentication       => :login,
  :user_name            => ENV['SMTP_USERNAME'],
  :password             => ENV['SMTP_PASSWORD'],
  :domain               => 'interserve.com',
  :enable_starttls_auto => true
}
