# Good advie: Improvise - Adapt - Overcome ..
# http://stackoverflow.com/a/29895985

ready = ->
  ContactPage =
    enableGoogleMapZoomOnClick: ->

      $('.contact-map').click ->
        $('.contact-map iframe').css "pointer-events", "auto"

      $( ".contact-map" ).mouseleave ->
        $('.contact-map iframe').css "pointer-events", "none"


  ContactPage.enableGoogleMapZoomOnClick()

# turbolinks ready event. It was page:load for Rails 4.
$(document).on 'turbolinks:load', ready
