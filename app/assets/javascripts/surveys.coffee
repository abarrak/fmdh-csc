ready = ->
  Survey =
    init: ->
      @setPopups()

    setPopups: ->
      popupElements = $ '[data-popup]'

      $.each popupElements, (index, el) ->
        popupTitle = ""
        data = $(el).data()

        $(el).on 'click', (e) ->
          e.preventDefault
          Survey.linkPopoup data.popupLink, data.popupTitle

    linkPopoup: (link, title) ->
      new_window = window.open link, title, 'toolbar=no, location=no, menubar=no, scrollbars=yes, resizable=no, width=850, height=685, top=30, left=150'

      new_window.focus()

  Survey.init()

# turbolinks ready event. It was page:load for Rails 4.
$(document).on 'turbolinks:load', ready
