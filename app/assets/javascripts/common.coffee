ready = ->
  App =
    init: ->
      @initalizejQ()
      @initializeClipBoard()

    # Initialization tasks for $
    initalizejQ: ->
      $('[data-toggle="tooltip"]').tooltip()

    # init clipboard.js
    initializeClipBoard: ->
      clipboard = new Clipboard('.btn-copy')
      clipboard.on 'success', (e) ->
        $(e.trigger).tooltip({ placement: 'bottom', title: 'Copied !' }).mouseover()
        # e.clearSelection()

      clipboard.on 'error', (e) ->
        $(e.trigger).tooltip({ placement: 'bottom', title: 'Copied failed :(' }).mouseover()

  App.init()

# turbolinks ready event. It was page:load for Rails 4.
$(document).on 'turbolinks:load', ready
