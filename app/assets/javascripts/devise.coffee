ready = ->
  LoginPage =

    validateFormInput: ->
      @form = $ "form.form-box"
      return if @form.length is 0

      @usernameField = $ "input[name='agent[username]']", @form
      @passwordField = $ "input[name='agent[password]']", @form
      return if @usernameField.length is 0 or @passwordField.length is 0

      $('button[type=submit]', @form).click (e) =>
        e.preventDefault()
        $('.callout').animate { opacity: 0, display: 'none' }, 300

        if not $.trim(@usernameField.val()) or not $.trim(@passwordField.val())
          $("#output").addClass("alert alert-danger animated fadeInUp").html "Please fill in login fields."
        else
          $("#output").removeClass("alert alert-danger animated fadeInUp").html ""
          $("#output").addClass("alert alert-success animated fadeInUp")
                      .html "Loging in .. Please wait .."
          @form.submit()

  LoginPage.validateFormInput()

# turbolinks ready event. It was page:load for Rails 4.
$(document).on 'turbolinks:load', ready

