module DashboardHelper
  def today_year
    Date.today.year
  end

  def today_month
    I18n.t("date.month_names")[Date.today.month]
  end

  def today_day
    Date.today.day
  end

  def surveys_count
    Survey.count
  end

  def filled_surveys_count
    Survey.where(filled: true).count
  end

  def survey_created_last_week_count
    Survey.where("created_at >= ?", 1.week.ago).count
  end

  def survey_filled_last_week_count
    Survey.where(filled: true).where("created_at >= ?", 1.week.ago).count
  end

  def last_sign_in
    last_sign = current_agent.try :last_sign_in_at
    "#{t 'pages.dashboard.your_last_sign_in'} #{time_ago_in_words last_sign} #{t 'ago'}"
  end
end
