module SurveysHelper
  def survey_breadcrumb_title survey
    "#{t 'surveys.singular'} #{t 'for'} #{t 'surveys.sr_with_no', no: survey.sr_number}"
  end

  def filled_status survey
    t survey.filled? ? '_yes' : '_no'
  end

  def filled_css_class survey
    survey.filled? ? 'blue-dark' : 'red-dark'
  end

  def preview_path survey
    preview_surveys_path @survey.unique_id
  end

  def preview_url survey
    preview_surveys_url unique_id: @survey.unique_id
  end

  def pad_when_little_records list
    return unless list.respond_to? :count

    records_pads = { 0 => 13, 1 => 11, 2 => 9, 3 => 6, 4 => 4, 5 => 2 }
    records_num = list.count

    records_pads[records_num].times { yield } if records_pads.key? records_num
  end

  def survey_form_into survey
    simple_format t('survey_questions.intro', no: survey.sr_number,
      sr_summary: survey.customer_request_summary.strip,
      sr_date: formatted_with_ago(survey.request_first_notification_date_and_time))
  end

  def get_alternative_lang format = :short
    get_t = -> (lang) { format == :long ? t("#{lang}_long") : t("#{lang}_short") }

    case I18n.locale
    when :en
      get_t.call 'arabic'
    when :ar
      get_t.call 'english'
    else
      raise Error.new 'Something went wrong.'
    end
  end

  def verify_rating_option attr
    return false if params[:survey_answer].blank?

    attr_paramed = params[:survey_answer][attr]
    attr_paramed && (1..5).include?(attr_paramed.to_i) ? true : false
  end

  def check_rating_option attr, value
    verify_rating_option(attr) && params[:survey_answer][attr].to_i == value ? true : false
  end

  def check_rating_option_for_radio attr, value
    check_rating_option(attr, value) ? "checked" : ""
  end

  def check_rating_option_in_css attr, value
    check_rating_option(attr, value) ? "active" : ""
  end

  def humanize_job_title survey
    Survey.human_job_title survey.customer_job_title
  end
end
