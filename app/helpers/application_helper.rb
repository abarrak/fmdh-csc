module ApplicationHelper
  # Is agent in any administrator role ?
  def admin?
    current_agent.try :admin?
  end

  # only for administrator (manager or deupty) role.
  def authenticate_admin!
    unless admin?
      flash[:warning] = t 'auth.no_premission'
      redirect_to surveys_url
    else
      true
    end
  end

  # Accessor for application name.
  def app_name
    CustomerSurveysCenter::Application.config.app_name
  end

  # Formulate a title combined with the base website title.
  def page_title title = ''
    base_title = t 'customer_surveys_center'
    (title.empty? ? base_title : "#{title} | #{base_title}")
  end

  def footer_year
    start   = 2016
    current = Date.today.year.to_i
    current == start ? start.to_s : "#{start} - #{current}"
  end

  def form_submit_button_text record
    record.new_record? ? t('forms.create') : t('forms.save')
  end

  def formatted datetime
    l datetime, format: :long unless datetime.blank?
  end

  def formatted_ago datetime
     "(#{time_ago_in_words(datetime).capitalize} #{t 'ago'})." unless datetime.blank?
  end

  def formatted_with_ago datetime
    unless datetime.blank?
     "#{formatted(datetime)} - (#{time_ago_in_words(datetime).capitalize} #{t 'ago'})"
    end
  end

  def formatted_short_date datetime
    l datetime, format: :short unless datetime.blank?
  end

  def greeting_phrase
    # initial imple: stackoverflow.com/a/32279088
    now = Time.now
    today = Date.today.to_time
    morning = today.beginning_of_day
    noon = today.noon
    evening = today.change hour: 17
    night = today.change hour: 20
    tomorrow = today.tomorrow

    if (morning..noon).cover? now then t 'good_morning'
    elsif (noon..evening).cover? now then t 'good_afternoon'
    elsif (evening..night).cover? now then t 'good_evening'
    elsif (night..tomorrow).cover? now then t 'good_night'
    end
  end

  # Gets the alternative locale other than the current.
  def other_locale
    I18n.locale == :en ? :ar : :en
  end

  # Returns css class for rtl langs only.
  def locale_css_class
    "rtl" if I18n.locale == :ar
  end
end
