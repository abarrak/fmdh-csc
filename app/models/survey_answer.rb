class SurveyAnswer
  include ActiveModel::Model
  extend ActiveModel::Callbacks
  include ActiveModel::Validations
  include ActiveModel::Validations::Callbacks
  include ActiveModel::AttributeMethods
  include ActiveModel::Serialization
  include ActiveModel::Serializers::JSON

  # Attributes
  define_attribute_methods :q1_answer, :q2_answer, :q3_answer, :q4_answer, :q5_answer
  attr_accessor :survey_unique_id, :sr_number,
                :q1_answer, :q2_answer, :q3_answer, :q4_answer, :q5_answer, :suggestions_and_notes

  # Validators
  validates :q1_answer, :q2_answer, :q3_answer, :q4_answer, :q5_answer,
            inclusion: { in: ["1", "2", "3", "4", "5"],
            message: I18n.t('.errors.form.pick_one_option_for_question') }

  # Callbacks

  # Business logic & methods.
  def attributes
    # serializable version of customer answers.
    { 'q1_answer' => q1_answer,
      'q2_answer' => q2_answer,
      'q3_answer' => q3_answer,
      'q4_answer' => q4_answer,
      'q5_answer' => q5_answer,
      'suggestions_and_notes' => suggestions_and_notes }
  end
end
