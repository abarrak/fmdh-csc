class Agent < ApplicationRecord
  # Include default devise modules.
  # Others available are: :confirmable, :timeoutable and :omniauthable.
  devise :database_authenticatable, :registerable, :lockable, :recoverable, :rememberable,
         :trackable, :validatable

  # Validators.
  validates :username, presence: true, length: { minimum: 3, maximum: 35 }
  validates :email, presence: true, length: { minimum: 6, maximum: 255 },
                    format: { with: /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i },
                    uniqueness: { case_sensitive: false }

  # Callbacks.
  before_save :normalize_attrs

  # Will_paginate setup.
  self.per_page = 12

  # Methods and business Logic.
  def normalize_attrs
    self.email.downcase!
    self.username.downcase!
  end

  def role
    admin? ? :admin : :agent
  end
end
