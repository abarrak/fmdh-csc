class Survey < ApplicationRecord
  # Relationships

  # Attributes
  serialize :answer
  enum customer_job_title: [:mr, :mrs, :ms, :eng, :dr]

  # Validators
  validates :fmdh_agent_name, presence: true, length: { minimum: 3, maximum: 40 }
  validates :customer_name,   presence: true, length: { minimum: 3, maximum: 40 }
  validates :sr_number, presence: true, uniqueness: true,
                        numericality: { only_integer: true, greater_than: 1800, less_than: 10000 }
  validates :customer_email, presence: true, length: { minimum: 6, maximum: 200 },
                    format: { with: /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i }
  validates :customer_phone_number, length: { minimum: 10, maximum: 15 }, allow_blank: true
  validates :customer_job_title, presence: true
  validates :request_first_notification_date_and_time, presence: true
  validates :initial_reply_email_date_and_time, presence: true
  validates :request_resolution_date, presence: true
  validates :customer_request_summary, presence: true, length: { minimum: 8, maximum: 10000 }
  validates :unique_id, presence: true, uniqueness: true

  # Callbacks
  before_validation :generate_unique_id
  before_save :timestamp_when_filled

  # Scopes
  default_scope -> { order id: :desc }

  # Will_paginate setup
  self.per_page = 25

  # Business logic & methods
  def conceal survey_answer
    if filled? || !self[:answer].blank?
      raise ActiveRecord::ReadOnlyRecord.new 'Survey is already answered'
    else
      self[:answer] = survey_answer.attributes
      filled_in
    end
  end

  def answer
    current = read_attribute :answer
    !current.nil? ? current.with_indifferent_access : write_attribute(:answer, {})
  end

  def self.job_titles_for_select
    customer_job_titles.collect do |title, _|
      [human_job_title(title), title]
    end
  end

  def self.human_job_title job_title
    self.human_enum_name :customer_job_title, job_title
  end

  private

  def generate_unique_id
    self[:unique_id] = random_token if self[:unique_id].nil? || self[:unique_id].length < 6
  end

  def random_token
    SecureRandom.urlsafe_base64 6
  end

  def filled_in
    self[:filled] = true
  end

  def timestamp_when_filled
    self[:filled_at] = DateTime.now if filled? && self[:filled_at].nil?
    # TODO: maybe cleae :filled_at if filled is return back to false.
    # But this behavior is really unexpected and defeat the pupose of asking customers to fill.
    # Mark: Delete this comment in next revision.
  end
end
