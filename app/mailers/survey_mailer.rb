class SurveyMailer < ApplicationMailer
  add_template_helper ApplicationHelper
  add_template_helper SurveysHelper

  SYSTEM_EMAIL = ENV['SMTP_USERNAME']
  # name: FMHelpDesk
  SYSTEM_EMAIL_WITH_NAME = %("Abdullah A." <#{SYSTEM_EMAIL}>)

  default from: SYSTEM_EMAIL_WITH_NAME, cc: SYSTEM_EMAIL_WITH_NAME

  def invite_email survey
    @survey = survey
    set_customer_to
    mail to: @customer_to,
         subject: t('survey_emails.invitation.subject', sr_number: @survey.sr_number)
  end

  def thank_email survey
    @survey = survey
    set_customer_to

    mail to: @customer_to,
         subject: t('survey_emails.thank.subject', sr_number: @survey.sr_number)
  end

  def fill_notification_email survey
    @survey = survey
    mail to: SYSTEM_EMAIL, subject: t('survey_emails.customer_filled.subject'), cc: nil
  end

  protected

  def set_customer_to
    @customer_to = %("#{@survey.customer_name}" <#{@survey.customer_email}>)
  end
end
