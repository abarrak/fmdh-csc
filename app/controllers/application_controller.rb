class ApplicationController < ActionController::Base
  include ApplicationHelper

  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :authenticate_agent!
  before_action :set_locale


  protected
    # permit devise params.
    def configure_permitted_parameters
      devise_parameter_sanitizer.permit :sign_up, keys: [:username]
      devise_parameter_sanitizer.permit :account_update, keys: [:username, :email, :password,
                                        :password_confirmation, :current_password]
    end

    def set_locale
      if params[:locale] && ['en', 'ar'].include?(params[:locale])
        I18n.locale = params[:locale]
      else
        I18n.locale = I18n.default_locale
      end
    end

    def default_url_options
      { locale: I18n.locale }
    end

    def after_sign_in_path_for resource
      dashboard_main_path
    end
end
