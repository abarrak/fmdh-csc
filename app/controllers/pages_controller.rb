class PagesController < ApplicationController
  skip_before_action :authenticate_agent!

  def home
  end

  def contact
  end
end
