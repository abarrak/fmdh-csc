class DashboardController < ApplicationController
  def main
  end

  def report
  end

  def stats
  end

  def settings
  end
end
