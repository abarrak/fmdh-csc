class SurveysController < ApplicationController
  # Action filters
  skip_before_action :authenticate_agent!, only: [:preview, :fill, :thank]
  before_action :authenticate_admin!, only: [:destroy]
  before_action :set_survey, only: [:show, :edit, :update, :destroy, :invitation, :invite]
  before_action :set_survey_by_unique_id, only: [:preview, :fill, :thank]

  # GET /surveys
  # GET /surveys.json
  def index
    @surveys = Survey.all.paginate(page: params[:page]).per_page records_count
  end

  # GET /surveys/1
  # GET /surveys/1.json
  def show
  end

  # GET /surveys/new
  def new
    @survey = Survey.new
  end

  # GET /surveys/1/edit
  def edit
  end

  # POST /surveys
  # POST /surveys.json
  def create
    @survey = Survey.new(survey_params)
    @job_title = params[:survey][:customer_job_title]
    puts "#" * 50
    puts @job_title
    puts "#" * 50
    respond_to do |format|
      if @survey.save
        format.html { redirect_to @survey,
                      notice: t('controller.created', m: t('surveys.singular')) }

        format.json { render :show, status: :created, location: @survey }
      else
        format.html { render :new }
        format.json { render json: @survey.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /surveys/1
  # PATCH/PUT /surveys/1.json
  def update
    respond_to do |format|
      if @survey.update(survey_params)
        format.html { redirect_to @survey,
                      notice: t('controller.updated', m: t('surveys.singular')) }
        format.json { render :show, status: :ok, location: @survey }
      else
        format.html { render :edit }
        format.json { render json: @survey.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /surveys/1
  # DELETE /surveys/1.json
  def destroy
    @survey.destroy
    respond_to do |format|
      format.html { redirect_to surveys_url,
                    notice: t('controller.destroyed', m: t('surveys.singular')) }
      format.json { head :no_content }
    end
  end

  # GET /surveys/preview/:unique_id
  # GET /surveys/preview/:unique_id.json
  # Shows the customer a survey in fill (write) mode.
  def preview
    set_survey_answer

    unless current_agent
      @survey.increment :hits_counter
      @survey.save
    end
  end

  # PATCH/PUT /surveys/fill
  # PATCH/PUT /surveys/fill.json
  # Persists the customer's survey fill.
  def fill
    @survey_answer = SurveyAnswer.new survey_answer_params

    respond_to do |format|
      if @survey.filled?
        flash[:info] = "Survey has already been filled."
        redirect_to(thank_surveys_url) && return
      end

      if @survey_answer.valid?
        format.html {
          conceal_and_persist
          SurveyMailer.thank_email(@survey).deliver_later
          SurveyMailer.fill_notification_email(@survey).deliver_later
          redirect_to thank_surveys_url, success: t('surveys.filled_successfully')
        }
        format.json { render :thank, status: :ok, location: @survey }
      else
        format.html { render :preview }
        format.json { render json: @survey_answer.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /surveys/:id/invitation
  # GET /surveys/:id/invitation.json
  # Shows the customer a survey in fill (write) mode.
  def invitation
    render :invitation, :layout => 'popup'
  end

  # POST /surveys/:id/invitation
  # POST /surveys/:id/invitation.json
  # Sends the invitation email.
  def invite
    SurveyMailer.invite_email(@survey).deliver_later
    render :invite, :layout => 'popup'
  end


  # GET /surveys/:unique_id/thank
  # GET /surveys/:unique_id/thank
  # Shows customer thank-you screen after successful survey fill.
  def thank
    unless @survey.filled?
      flash[:danger] = "Survey is not filled. Please complete it before proceeding."
      redirect_to preview_surveys_url
    else
      render :thank
    end
  end

  private

  def set_survey
    @survey = Survey.find params[:id]
  end

  def set_survey_by_unique_id
    @survey = Survey.find_by! unique_id: params[:unique_id]
  end

  def set_survey_answer
    @survey_answer = @survey.filled? ? SurveyAnswer.new(@survey.answer) : SurveyAnswer.new
    @survey_answer.survey_unique_id = @survey.unique_id
    @survey_answer.sr_number = @survey.sr_number
  end

  def survey_params
    params.require(:survey).permit :fmdh_agent_name, :customer_name, :customer_phone_number,
      :sr_number, :request_first_notification_date_and_time, :initial_reply_email_date_and_time,
      :request_resolution_date, :customer_request_summary, :customer_email, :customer_job_title
  end

  def survey_answer_params
    params.require(:survey_answer).permit :q1_answer, :q2_answer, :q3_answer, :q4_answer, :q5_answer,
      :suggestions_and_notes, :unique_id
  end

  def records_count
    !params[:count].blank? ? params[:count].to_i : Survey.per_page
  end

  def conceal_and_persist
    @survey.conceal @survey_answer
    @survey.save
  end
end
